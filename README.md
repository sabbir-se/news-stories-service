# News Stories Service

**Technologies:**

* JDK 1.8
* Jersey 2.11
* Apache Maven
* Apache Tomcat
* IntelIj


**Steps:**

* First Import the project as an existing maven project.
* Set the log file location ('./src/main/resources/log4j.properties').
> log4j.appender.file.File = _yourDesiredFileLocation
>
> **Note:** Set your location without underscore.

* Set your desired file location where all files will be stored. Use json format to store the content in the file. ('./src/main/resources/application.properties').
> file.location = _yourDesiredFileLocation
>
> **Note:** Set your location without underscore.

* Then build the projects using maven tools, like: 'mvn clean install'.
* Then deploy the war into apache tomcat.
* Visit: http://localhost:8080/news and you should be all set.
 
 
**Exception Handling:**

* Here I implements a custom exception handler. When an exception occurs, my custom exception handler catch the exception and return it by a custom format. The format will be like this:
> errorCode
>
> errorMessage
>
> In that way, what type of error occurs that will be easily understandable.

* All error message are return in JSON format. like:
>   {
        "errorCode": "story_service_001",
        "errorMessage": "/home/sabbir/.json (No such file or directory)"
    }
   
**API:**

* Create story 

> method: POST
>
> url: http://localhost:8080/news/api/v1/story
>
> header:
>
>    key -> Content-Type
>
>    value -> "application/json"
>
> body:
    {
        "title": "Comet robot Philae phones home again",
        "body": "Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space  Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.",
        "author": "Jonathan Amos"
    }
>
> response: If success, return the story object. Otherwise, return the custom error object. (output will be in JSON format)
>
> **Note:** Generate a random Id for a story. Create the file with that random Id. like: f113020d-8057-4729-90c2-233dbbae2d44.json
    
    
* Read story

> method: GET
>
> url: http://localhost:8080/news/api/v1/story/{story_id}  here, {story_id} means a story Id which is generated when created a story.
>
> header:
>
>   key -> Content-Type
>
>   value -> "application/json" / "text/plain"
>
> response: If success, return type is depends on content-type. If content-type is "application/json", then return the JSON format output else return the text format output. Otherwise return the custom error object. 
>
> JSON output like:
> {
    "id": "f113020d-8057-4729-90c2-233dbbae2d44",
    "title": "Comet robot Philae phones home again",
    "body": "Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space  Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.",
    "author": "Jonathan Amos",
    "createdDate": "2016-09-05 17:43:08",
    "modifiedDate": "2016-09-05 17:43:08"
  }
>
> TEXT output like:
>
> id:f113020d-8057-4729-90c2-233dbbae2d44
>
> title:Comet robot Philae phones home again
>
> body:Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space  Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.
>
> author:Jonathan Amos
>
> createdDate:2016-09-05 17:43:08
>
> modifiedDate:2016-09-05 17:43:08

* Update story

> method: PUT
>
> url: http://localhost:8080/news/api/v1/story/{story_id}  here, {story_id} means a story Id which is generated when created a story.
>
> header:
>
>   key -> Content-Type
>
>   value -> "application/json" / "text/plain"
>
> body:
> 
> For "application/json": 
    {
        "title": "Comet robot Philae phones home again",
        "body": "Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space  Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.",
        "author": "Jonathan Amos"
    }
>
> For "text/plain":
>
> title:Comet robot Philae phones home again
>
> body:Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space  Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.
>
> author:Jonathan Amos
>
> response: If success, return the story object. Otherwise return the custom error object. (JSON format output)

* Feed from story

> method: GET
>
> url: http://localhost:8080/news/api/v1/story/feed 
>
> header:
>
>   key -> Content-Type
>
>   value -> "application/xml"
>
> response: response: If success, return all the stories in the system as an RSS feed. Output format is XML. Otherwise return the custom error object. 
>
> like:
>
> [
  <?xml version="1.0" encoding="UTF-8"?>
  <rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
      <channel>
          <title>Comet robot Philae phones home again</title>
          <link>http://localhost:8080/news/api/v1/story/feed</link>
          <description>Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.</description>
          <pubDate>Mon, 05 Sep 2016 17:38:18 GMT</pubDate>
          <dc:creator>Jonathan Amos</dc:creator>
          <dc:date>2016-09-05T17:38:18Z</dc:date>
      </channel>
  </rss>,
>
  <?xml version="1.0" encoding="UTF-8"?>
  <rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
      <channel>
          <title>Sabbir robot Philae phones home again</title>
          <link>http://localhost:8080/news/api/v1/story/feed</link>
          <description>Europe’s comet lander has again been in touch with Earth. The Philae probe made three short contacts of about 10 seconds each at roughly 2130 GMT on Sunday. Controllers at the European Space Agency said the contacts were briefer than they had hoped, but proved the little robot was in encouragingly good health after its seven-month slumber. Philae landed on Comet 67P in November and worked for 60 hours before its battery ran flat.</description>
          <pubDate>Mon, 05 Sep 2016 17:38:57 GMT</pubDate>
          <dc:creator>Sabbir Ahmed</dc:creator>
          <dc:date>2016-09-05T17:38:57Z</dc:date>
      </channel>
  </rss> 
  ]
    
# End