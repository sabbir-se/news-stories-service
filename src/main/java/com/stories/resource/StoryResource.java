package com.stories.resource;

import com.stories.exception.CustomException;
import com.stories.model.Story;
import com.stories.service.StoryService;
import com.stories.service.impl.StoryServiceImpl;
import com.stories.util.Constant;
import com.stories.util.Utility;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by sabbir on 9/2/16.
 */

@Path("/v1/story")
public class StoryResource {

    private static final Logger logger = Logger.getLogger(StoryResource.class);

    private static final StoryService storyService = new StoryServiceImpl();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createStory(Story story) throws CustomException {

        logger.info("Story create:: Start");
        Story createdStory = storyService.saveStory(story);
        logger.info("Story create:: End");

        return Response.ok().entity(createdStory).build();
    }

    @PUT
    @Path("/{story_id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response updateStory(@PathParam("story_id") String storyID, String requestBody,
                                @Context HttpHeaders headers) throws CustomException {

        String contentType = headers.getHeaderString(Constant.CONTENT_TYPE);
        logger.info("Content type name: " + contentType);

        logger.info("Update story:: Start");
        Story updatedStory = storyService.updateStory(storyID, requestBody, contentType);
        logger.info("Update story:: End");

        return Response.ok().entity(updatedStory).build();
    }

    @GET
    @Path("/{story_id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getStory(@PathParam("story_id") String storyID,
                             @Context HttpHeaders headers) throws CustomException {

        String contentType = headers.getHeaderString(Constant.CONTENT_TYPE);
        logger.info("Content type name: " + contentType);

        logger.info("Read story:: Start");
        Story story = storyService.getStory(storyID);
        logger.info("Read story:: End");

        if(contentType.equals(Constant.JSON_CONTENT_TYPE)){
            return Response.ok().entity(story).build();
        }
        return Response.ok().entity(Utility.getTextFromModel(story)).build();
    }

    @GET
    @Path("/feed")
    @Produces(MediaType.APPLICATION_XML)
    public Response readAllFeeds() throws CustomException {

        logger.info("Read feed:: Start");
        List<String> feedList = storyService.getAllFeeds();
        logger.info("Feed list size: " + feedList.size());
        logger.info("Read feed:: End");

        return Response.ok().entity(feedList.toString()).build();
    }
}