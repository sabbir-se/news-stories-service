package com.stories.service.impl;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Sabbir on 03/09/2016.
 */
public class ApplicationProperties {

    private static final Logger logger = Logger.getLogger(ApplicationProperties.class);

    private static final String PROPERTIES_FILE = "application.properties";
    private static final Properties appProp = new Properties();

    static {
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream propIS = loader.getResourceAsStream(PROPERTIES_FILE);
            appProp.load(propIS);

        } catch (IOException e) {
            logger.error("An error occurred while loading application properties: " + e.getMessage());
        }
    }

    public static final String FILE_LOCATION = appProp.getProperty("file.location");

    static final String FEED_LINK = appProp.getProperty("feed.link");
    static final String FEED_TYPE = appProp.getProperty("feed.type");
}
