package com.stories.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stories.exception.CustomException;
import com.stories.exception.ErrorMessage;
import com.stories.model.Story;
import com.stories.service.StoryService;
import com.stories.util.Constant;
import com.stories.util.Utility;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.SyndFeedOutput;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Sabbir on 04/09/2016.
 */
public class StoryServiceImpl implements StoryService {

    private static final Logger logger = Logger.getLogger(StoryServiceImpl.class);

    /**
     * Save news stories into File system.
     * When creating a story, a random ID will be generated
     * for storyID and file name will be the storyID + file extension (.json)
     *
     * @param story This is an object with title, author, body member variables.
     * @return      Created story object
     */
    @Override
    public Story saveStory(Story story) throws CustomException {
        File file;
        ObjectMapper mapper = new ObjectMapper();
        try {
            String storyId = Utility.generateRandomString();
            logger.info("Generated story ID: " + storyId);

            logger.info("File path name: " + Utility.filePathName(storyId));
            file = new File(Utility.filePathName(storyId));

            story.setId(storyId);
            story.setCreatedDate(Utility.getCurrentDate());
            story.setModifiedDate(Utility.getCurrentDate());
            mapper.writeValue(file, story);
            logger.info("Successfully store the story into a file");

            return story;

        } catch (Exception e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_001, e.getMessage());
            throw new CustomException(errorMessage);
        }
    }

    /**
     * Update news stories in a specific file.
     *
     * @param storyID This is a storyID. Also means a fileID.
     * @param requestBody This is the content of the stories,
     *                    which are going to be updated by removing previous data.
     * @param contentType This is headers.
     * @return      Updated story object
     */
    @Override
    public Story updateStory(String storyID, String requestBody, String contentType) throws CustomException {
        File file;
        Story existStory, updateStory;
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("File path name: " + Utility.filePathName(storyID));
            file = new File(Utility.filePathName(storyID));

            existStory = mapper.readValue(file, Story.class);
            if(existStory == null){
                ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_002,
                        Constant.STORY_SERVICE_002_DESCRIPTION);
                throw new CustomException(errorMessage);
            }

            if(contentType.equals(Constant.JSON_CONTENT_TYPE)) {
                updateStory = mapper.readValue(requestBody, Story.class);

            } else {
                updateStory = Utility.getModelFromText(requestBody);
            }

            if(updateStory == null){
                ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_003,
                        Constant.STORY_SERVICE_003_DESCRIPTION);
                throw new CustomException(errorMessage);
            }

            updateStory.setId(existStory.getId());
            updateStory.setCreatedDate(existStory.getCreatedDate());
            updateStory.setModifiedDate(Utility.getCurrentDate());

            mapper.writeValue(file, updateStory);
            logger.info("Successfully update the story.");

            return updateStory;

        } catch (IOException e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_001, e.getMessage());
            throw new CustomException(errorMessage);
        }
    }

    /**
     * Read news stories in a specific file.
     *
     * @param storyID This is a storyID. Also means a fileID.
     * @return      Read story object
     */
    @Override
    public Story getStory(String storyID) throws CustomException {
        File file;
        Story story;
        ObjectMapper mapper = new ObjectMapper();
        try{
            logger.info("File path name: " + Utility.filePathName(storyID));
            file = new File(Utility.filePathName(storyID));

            story = mapper.readValue(file, Story.class);
            if(story == null){
                ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_002,
                        Constant.STORY_SERVICE_002_DESCRIPTION);
                throw new CustomException(errorMessage);
            }
            return story;

        } catch (IOException e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_001, e.getMessage());
            throw new CustomException(errorMessage);
        }
    }

    /**
     * Read all the content in the file system as an valid RSS feed.
     *
     * @return  All the stories as an RSS feed
     */
    @Override
    public List<String> getAllFeeds() throws CustomException {

        File file;
        File[] allFiles;
        SyndFeed feed;
        Writer writer;
        SyndFeedOutput output;
        List<String> feeds;
        try {
            logger.info("Storage location: " + ApplicationProperties.FILE_LOCATION);
            file = new File(ApplicationProperties.FILE_LOCATION);
            allFiles = file.listFiles();
            if (allFiles != null) {
                feeds = new ArrayList<>();

                ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
                List<Future<Story>> resultList = new ArrayList<>();

                logger.info("File list size: " + allFiles.length);
                for (File aFile : allFiles) {
                    logger.info("File name: " + aFile.getName());

                    ReadFromFile readFromFile = new ReadFromFile(aFile);
                    Future<Story> result = executor.submit(readFromFile);
                    resultList.add(result);
                }

                // now retrieve the result
                for(Future<Story> future : resultList){
                    Story story = future.get();

                    feed = generateFeed(story);
                    output = new SyndFeedOutput();
                    writer = new StringWriter();
                    output.output(feed, writer);

                    feeds.add(writer.toString());
                    writer.close();
                }
                // This will make the executor accept no new threads
                // and finish all existing threads in the queue
                executor.shutdown();

                return feeds;

            } else {
                ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_005,
                        Constant.STORY_SERVICE_005_DESCRIPTION);
                throw new CustomException(errorMessage);
            }

        } catch (Exception e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_001, e.getMessage());
            throw new CustomException(errorMessage);
        }
    }

    /**
     * Generate feed from story.
     *
     * @param story This is story object, convert it as an valid RSS feed
     * @return      Valid RSS feed from the story
     */
    private SyndFeed generateFeed(Story story) throws CustomException {

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.DATE_FORMAT);
        try {
            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType(ApplicationProperties.FEED_TYPE);
            feed.setTitle(story.getTitle());
            feed.setLink(ApplicationProperties.FEED_LINK);
            feed.setAuthor(story.getAuthor());
            feed.setDescription(story.getBody());
            feed.setPublishedDate(dateFormat.parse(story.getCreatedDate()));

            return feed;

        } catch (ParseException e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_004,
                    Constant.STORY_SERVICE_004_DESCRIPTION);
            throw new CustomException(errorMessage);
        }
    }
}
