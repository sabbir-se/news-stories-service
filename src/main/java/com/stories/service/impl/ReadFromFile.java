package com.stories.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stories.exception.CustomException;
import com.stories.exception.ErrorMessage;
import com.stories.model.Story;
import com.stories.util.Constant;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * Created by sabbir on 9/5/16.
 */
public class ReadFromFile implements Callable<Story> {

    private File file;
    private ObjectMapper mapper;

    /**
     * Class constructor with file objects.
     */
    ReadFromFile(File file) {
        this.file = file;

        mapper = new ObjectMapper();
    }

    /**
     * Convert the file content into Story object.
     *
     * @return Story object from the file system
     */
    @Override
    public Story call() throws CustomException {

        try{
            return mapper.readValue(file, Story.class);

        } catch (IOException e) {
            ErrorMessage errorMessage = new ErrorMessage(Constant.STORY_SERVICE_001, e.getMessage());
            throw new CustomException(errorMessage);
        }
    }
}
