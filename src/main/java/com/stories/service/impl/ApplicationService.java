package com.stories.service.impl;

import com.stories.filter.ResponseCORSFilter;
import com.stories.resource.StoryResource;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by Sabbir on 03/09/2016.
 */
public class ApplicationService extends ResourceConfig {

    public ApplicationService(){
        packages(StoryResource.class.getPackage().getName());
        register(ResponseCORSFilter.class);
    }
}
