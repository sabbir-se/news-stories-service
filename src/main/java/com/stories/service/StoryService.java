package com.stories.service;

import com.stories.exception.CustomException;
import com.stories.model.Story;

import java.util.List;

/**
 * Created by Sabbir on 03/09/2016.
 */
public interface StoryService {

    Story saveStory(Story story) throws CustomException;

    Story updateStory(String storyID, String requestBody,
                      String contentType) throws CustomException;

    Story getStory(String storyID) throws CustomException;

    List<String> getAllFeeds() throws CustomException;
}
