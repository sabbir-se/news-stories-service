package com.stories.util;

/**
 * Created by Sabbir on 03/09/2016.
 */
public class Constant {

    static final String ID = "id";
    static final String TITLE = "title";
    static final String BODY = "body";
    static final String AUTHOR = "author";
    static final String CREATED_DATE = "createdDate";
    static final String MODIFIED_DATE = "modifiedDate";

    static final String COLON_SIGN = ":";
    static final String NEW_LINE = "\n";

    static final String FILE_EXTENSION_NAME = ".json";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String JSON_CONTENT_TYPE = "application/json";

    public static final String STORY_SERVICE_001 = "story_service_001";

    public static final String STORY_SERVICE_002 = "story_service_002";
    public static final String STORY_SERVICE_002_DESCRIPTION = "story not found.";

    public static final String STORY_SERVICE_003 = "story_service_003";
    public static final String STORY_SERVICE_003_DESCRIPTION = "story format incorrect. " +
            "please check content-type & request body.";

    public static final String STORY_SERVICE_004 = "story_service_004";
    public static final String STORY_SERVICE_004_DESCRIPTION = "date parse error.";

    public static final String STORY_SERVICE_005 = "story_service_005";
    public static final String STORY_SERVICE_005_DESCRIPTION = "feed not found.";
}
