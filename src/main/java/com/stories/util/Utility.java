package com.stories.util;

import com.stories.model.Story;
import com.stories.service.impl.ApplicationProperties;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Sabbir on 03/09/2016.
 */
public class Utility {

    public static boolean isNullOrEmpty(String s){
        return s == null || s.isEmpty();
    }

    public static boolean isNullOrEmpty(List list){
        return list == null || list.size() == 0;
    }

    // Get random generated string
    public static String generateRandomString(){
        return UUID.randomUUID().toString();
    }

    // Get current date
    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat(Constant.DATE_FORMAT);
        Date date = new Date();
        return dateFormat.format(date);
    }

    // Absolute file path
    public static String filePathName(String storyID){
        return ApplicationProperties.FILE_LOCATION + storyID + Constant.FILE_EXTENSION_NAME;
    }

    // Convert model to text format
    public static String getTextFromModel(Story story) {

        return Constant.ID + Constant.COLON_SIGN + story.getId() +
                Constant.NEW_LINE +
                Constant.TITLE + Constant.COLON_SIGN + story.getTitle() +
                Constant.NEW_LINE +
                Constant.BODY + Constant.COLON_SIGN + story.getBody() +
                Constant.NEW_LINE +
                Constant.AUTHOR + Constant.COLON_SIGN + story.getAuthor() +
                Constant.NEW_LINE +
                Constant.CREATED_DATE + Constant.COLON_SIGN + story.getCreatedDate() +
                Constant.NEW_LINE +
                Constant.MODIFIED_DATE + Constant.COLON_SIGN + story.getModifiedDate();
    }

    // Convert text format to model
    public static Story getModelFromText(String body) {
        Story story = new Story();
        String[] lineSplit = body.split(Constant.NEW_LINE);
        for (String aLineSplit : lineSplit) {
            String[] bodySplit = aLineSplit.split(Constant.COLON_SIGN);
            if (bodySplit[0].equals(Constant.TITLE)) {
                story.setTitle(bodySplit[1]);
            }

            if (bodySplit[0].equals(Constant.BODY)) {
                story.setBody(bodySplit[1]);
            }

            if (bodySplit[0].equals(Constant.AUTHOR)) {
                story.setAuthor(bodySplit[1]);
            }
        }

        if(story.getTitle() == null && story.getAuthor() == null
                && story.getBody() == null) {
            return null;
        }
        return story;
    }
}
